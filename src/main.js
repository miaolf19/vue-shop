import '@/assets/css/global.css'
import '@/assets/fonts/iconfont.css'
import axios from 'axios'
import moment from 'moment'
import 'quill/dist/quill.bubble.css'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import Vue from 'vue'
import VueQuillEditor from 'vue-quill-editor'
import TreeTable from 'vue-table-with-tree-grid'
import App from './App.vue'
import './plugins/element.js'
import router from './router'

Vue.config.productionTip = false
axios.interceptors.request.use((config) => {
  config.headers.Authorization = window.sessionStorage.getItem('token')
  console.log(config)
  return config
})
Vue.use(VueQuillEditor)
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
Vue.prototype.$http = axios
Vue.component('tree-table', TreeTable)
Vue.filter('dateFormat', function (time) {
  return moment(time).format('YYYY-MM-DD hh:mm:ss')
})
new Vue({
  router,
  render: (h) => h(App)
}).$mount('#app')
