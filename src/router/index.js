import Add from '@/components/Goods/Add.vue'
import Cate from '@/components/Goods/Cate.vue'
import GoodsList from '@/components/Goods/List.vue'
import Params from '@/components/Goods/Params.vue'
import Home from '@/components/Home.vue'
import Login from '@/components/Login.vue'
import Order from '@/components/Order/order.vue'
import Rights from '@/components/Power/Rights.vue'
import Roles from '@/components/Power/Roles.vue'
// import HomeView from '../views/HomeView.vue'
import Report from '@/components/Report/Report.vue'
import Users from '@/components/User/Users.vue'
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/login' },
  { path: '/login', component: Login },
  {
    path: '/home',
    component: Home,
    redirect: '/users',
    children: [
      // { path: '/welcome', component: Welcome },
      { path: '/users', component: Users },
      { path: '/rights', component: Rights },
      { path: '/roles', component: Roles },
      { path: '/categories', component: Cate },
      { path: '/goods', component: GoodsList },
      { path: '/goods/add', component: Add },
      { path: '/params', component: Params },
      { path: '/orders', component: Order },
      { path: '/reports', component: Report }
    ]
  }
]

const router = new VueRouter({
  routes
})
//挂载路由导航守卫
router.beforeEach((to, from, next) => {
  if (to.path === '/login') return next()
  // 获取token
  const token = window.sessionStorage.getItem('token')
  if (!token) return next('/login')
  next()
})
export default router
